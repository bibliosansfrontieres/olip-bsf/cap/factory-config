#!/bin/bash

# This script soft resets the CAP and formats the HDD.
# Usage:
#   reset.sh                  Keep the BSF access means (SSH keys, VPN)
#   reset.sh rm-bsf-access    Remove the BSF access means (SSH keys, VPN)

say() {
  >&2 echo "[+] $*"
}

if [ "$(id -u)" != 0 ]; then
  say "Error: this script must be run as root."
  exit 13  # EACCES
fi

if [ "$( dmidecode -t system | awk ' /Product Name/ { print $3 } ' )" != "CMAL" ] ; then
  say "Error: this script must be used on CMAL (CAP) devices only!"
  exit 19  # ENODEV
fi

rm_bsf_access="false"
[ "$1" == "rm-bsf-access" ] && rm_bsf_access="true"

#
# functions
#

save_tinc_interface() {
  say "Saving tinc interface..."
  tinc_ifname=$( uci get network.tinc.ifname )
  tinc_macaddr=$( uci get network.tinc.macaddr )
}
restore_factory_config() {
  say "Downloading and restoring default configuration..."
  fctarballurl='https://bibliosansfrontieres.gitlab.io/olip/devices/cap/factory-config/factory-config.tar.bz2'
  until wget --quiet "$fctarballurl" \
    -O /root/factory-config.tar.bz2 ; do sleep 1 ; done
  cd / && tar xvfjp /root/factory-config.tar.bz2
}
restore_tinc_interface() {
  say "Restoring tinc configuration..."
  uci set network.tinc='interface'
  uci set network.tinc.ifname="${tinc_ifname}"
  uci set network.tinc.proto='dhcp'
  uci set network.tinc.macaddr="${tinc_macaddr}"
  uci commit network
}
remove_hotplug_hooks() {
  say "Removing hotplug hooks..."
  for files in 45-pull-containers 49-cache-server 50-update-content 60-push-log
  do
    rm -f /etc/hotplug.d/iface/$files
  done
}
reset_password() {
  say "Resetting password for user cap..."
  # shellcheck disable=SC2016 # it's not a variable and doesn't need expanding
  usermod --password '$1$.SwYxBkA$sIY5tCkbXGeK/cl/VcnRf0' cap
}
remove_local_ssh_key_pairs() {
  say "Removing SSH keys..."
  rm -f /root/.ssh/id_rsa /root/.ssh/id_rsa.pub /root/.ssh/{idb,idc}-*-*
  rm -f /home/cap/.ssh/id_rsa /home/cap/.ssh/id_rsa.pub
}
remove_local_ssh_authorized_keys() {
  say "Removing SSH authorized_keys files..."
  rm -f /root/.ssh/authorized_keys
  rm -f /home/cap/.ssh/authorized_keys
}
disable_tinc() {
  say "Disabling Tinc..."
  systemctl stop tinc@testvpn.service
  systemctl disable tinc@testvpn.service
  rm -rf /etc/tinc/
}
disable_systemd_timers() {
  for i in \
    olip_database_upgrade.timer \
    is-cache-server-available.timer \
    olip_descriptor_update.timer \
    olip_container_upgrade.timer \
    push_log.timer \
    ; do
      systemctl disable $i &> /dev/null ;
      systemctl stop    $i &> /dev/null ;
    done
}
disable_balena_engine() {
  say "Disabling balena-engine..."
  systemctl stop balena-engine
  systemctl disable balena-engine
  rm -rf /var/lib/balena-engine/
}
clean_deploy_scripts() {
  say "Removing Deploy scripts and flags..."
  rm -f /var/spool/cron/crontabs/root
  rm -f /usr/local/bin/initcap.py
  rm -f /usr/local/bin/callback.py
  rm -f /usr/local/bin/deploy.py
  rm -f -r /opt/*
}
clean_cronjobs() {
  say "Removing old fashioned cronjobs..."
  rm -f /etc/cron.d/descriptor-update
  rm -f /etc/cron.d/olip-update-stack
}
clean_olip_configuration() {
  say "Removing OLIP configuration file..."
  rm -f /etc/default/olip
}
clean_data_volume() {
  say "Cleaning data volume..."
  rm -rf /data
  umount -A -l /dev/sda1
  mkfs.ext4 -F /dev/sda1
}
get_default_mac_address() {
  say "Retrieving ethernet MAC address..."
  eth_sys_path=$( find /sys/class/net/ | grep -m1 'enp2s0' )
  eth_macaddr=$( awk -F":" '{ print $5 $6 }' "$eth_sys_path/address" )
}
reset_hostname() {
  say "Resetting the hostname to the factory one... (/etc/hostname, /etc/hosts)"
  echo "CMAL-${eth_macaddr}" > /etc/hostname
  sed -i -e "s,$( awk -F" " ' /127.0.1.1/ { print $2 }' /etc/hosts ),CMAL-${eth_macaddr},g" /etc/hosts
  say "Resetting the content host name... (uci)"
  uci set system.@system[0].hostname=my.content
  uci commit system
}
reset_ssid() {
  say "Resetting the SSID..."
  uci set wireless.@wifi-iface[0].ssid="CMAL-2.4G-${eth_macaddr}"
  uci set wireless.@wifi-iface[1].ssid="CMAL-5G-${eth_macaddr}"
  uci commit wireless
}
logrotate_ansible_logfile() {
  [ -f /etc/logrotate.d/ansible-pull ] || return
  say "Rotating the ansible-pull.log..."
  logrotate -f /etc/logrotate.d/ansible-pull
}
remove_leftovers() {
  # Some rare devices may have leftovers from experiments
  # Better safe than sorry; remove'em'all anyway
  say "Removing leftovers..."
  # Use the `-v` switch so we have a visual hint about what was found and removed
  rm -v -f /etc/apt/sources.list.d/apt_syncthing_net.list
}

#
# main
#

[ $rm_bsf_access = "false" ] && save_tinc_interface

# Erase the current configuration with a backup
restore_factory_config
# But restore the tinc interface
[ $rm_bsf_access = "false" ] && restore_tinc_interface

remove_leftovers

remove_hotplug_hooks

[ $rm_bsf_access = "true" ] && {
  reset_password
  remove_local_ssh_key_pairs
  remove_local_ssh_authorized_keys
  disable_tinc
}

disable_systemd_timers
disable_balena_engine

clean_deploy_scripts
clean_cronjobs
clean_olip_configuration

clean_data_volume

get_default_mac_address
reset_hostname
reset_ssid

logrotate_ansible_logfile

say "Done. You should reboot the device."
